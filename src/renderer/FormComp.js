import styled from 'styled-components';
import { useForm } from 'react-hook-form';

// Create a styled form component
const StyledForm = styled.form`
  max-width: 400px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ddd;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);

  input {
    width: 100%;
    margin-bottom: 10px;
    padding: 8px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;

    &:last-child {
      margin-bottom: 0;
    }
  }

  span {
    color: red;
  }

  input[type="submit"] {
    background-color: black ; 
    color: white;
    cursor: pointer;
  }
`;

export default function FormComp() {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);

  console.log(watch('example')); // watch input value by passing the name of it

  return (
    <>
    <h3> Enter your blog data: </h3>
    <StyledForm onSubmit={handleSubmit(onSubmit)}>
      <label>Blog Name</label>
      
      <input defaultValue="test" {...register('example')} />
      <input {...register('exampleRequired', { required: true })} />
      {errors.exampleRequired && <span>This field is required</span>}
      <input type="submit" value="Submit" />
    </StyledForm>
    </>
  );
}
